//
//  ViewController.swift
//  Barta
//
//  Created by apple on 3/1/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //    let bgColor = CGColor(srgbRed: 235, green: 76, blue: 119, alpha: 1.0)
    //    let bColor = UIColor(red: 235, green: 76, blue: 119, alpha: 1.0)
    
    //MARK:- Utilities
    
    var Button = UIButton()
    let bartaVC = BartaViewController()
    private let loginContentView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .gray
        return view
    }()
    private let unameTxtField:UITextField = {
        let txtField = UITextField()
        txtField.translatesAutoresizingMaskIntoConstraints = false
        txtField.backgroundColor = .white
        txtField.borderStyle = .roundedRect
        return txtField
    }()
    private let pwordTxtField:UITextField = {
        let txtField = UITextField()
        txtField.translatesAutoresizingMaskIntoConstraints = false
        txtField.borderStyle = .roundedRect
        return txtField
    }()
    let btnLogin:UIButton = {
        let btn = UIButton(type:.system)
        btn.backgroundColor = .blue
        btn.setTitle("Login", for: .normal)
        btn.tintColor = .white
        btn.layer.cornerRadius = 7
        btn.clipsToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    //MARK:- init
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .purple
        //MARK:- Logi In View
        view.addSubview(loginContentView)
        loginContentView.addSubview(unameTxtField)
        loginContentView.addSubview(pwordTxtField)
        loginContentView.addSubview(btnLogin)
        loginContentView.leftAnchor.constraint(equalTo:view.leftAnchor).isActive = true
        loginContentView.rightAnchor.constraint(equalTo:view.rightAnchor).isActive = true
        loginContentView.heightAnchor.constraint(equalToConstant: view.frame.height/3).isActive = true
        loginContentView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        //MARK:- Username Text Field
        //unameTxtField.topAnchor.constraint(equalTo:loginContentView.topAnchor).isActive = true
        unameTxtField.topAnchor.constraint(equalTo:loginContentView.topAnchor, constant:50).isActive = true
        unameTxtField.placeholder = "Enter your username"
        //      unameTxtField.leftAnchor.constraint(equalTo:loginContentView.leftAnchor).isActive = true
        //      unameTxtField.rightAnchor.constraint(equalTo:loginContentView.rightAnchor).isActive = true
        unameTxtField.leftAnchor.constraint(equalTo:loginContentView.leftAnchor, constant:20).isActive = true
        unameTxtField.rightAnchor.constraint(equalTo:loginContentView.rightAnchor, constant:-20).isActive = true
        unameTxtField.heightAnchor.constraint(equalToConstant:50).isActive = true
        //MARK:- Password Text Field
        pwordTxtField.leftAnchor.constraint(equalTo:loginContentView.leftAnchor, constant:20).isActive = true
        pwordTxtField.rightAnchor.constraint(equalTo:loginContentView.rightAnchor, constant:-20).isActive = true
        pwordTxtField.heightAnchor.constraint(equalToConstant:50).isActive = true
        pwordTxtField.topAnchor.constraint(equalTo:unameTxtField.bottomAnchor, constant:20).isActive = true
        pwordTxtField.placeholder = "Enter your password"
        //MARK:-Login Button
        btnLogin.topAnchor.constraint(equalTo:pwordTxtField.bottomAnchor, constant:20).isActive = true
        btnLogin.leftAnchor.constraint(equalTo:loginContentView.leftAnchor, constant:20).isActive = true
        btnLogin.rightAnchor.constraint(equalTo:loginContentView.rightAnchor, constant:-20).isActive = true
        btnLogin.heightAnchor.constraint(equalToConstant:50).isActive = true
        // Do any additional setup after loading the view.
    }
    
    
}
